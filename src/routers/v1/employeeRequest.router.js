const express = require("express");
const employeeRequestController = require("../../controllers/employeeRequest.controller");
const employeeRequestRouter = express.Router();

employeeRequestRouter.get(
  "",
  employeeRequestController.getAllEmployeeRequest()
);
employeeRequestRouter.post(
  "",
  employeeRequestController.createEmployeeRequest()
);
employeeRequestRouter.put(
  "/:requestID",
  employeeRequestController.updateEmployeeRequest()
);
employeeRequestRouter.delete(
  "/:requestID",
  employeeRequestController.deleteEmployeeRequest()
);

module.exports = employeeRequestRouter;
