// router v1
const express = require("express");

const userController = require("../../controllers/user.controller");
const authController = require("../../controllers/auth.controller");
const businessUnitController = require("../../controllers/businessUnitl.controller");
const sectionController = require("../../controllers/section.controller");
const employeeController = require("../../controllers/employee.controller");
const roleController = require("../../controllers/role.controller");
const employeeRequestController = require("../../controllers/employeeRequest.controller");
const authorization = require("../../middleware/authorization");

// path v1: /api/v1
const v1 = express.Router();

v1.post("/login", authController.login());
v1.post("/checkPassword", authController.checkPassword());
v1.post("/ChangePassword", authController.ChangePassword());
v1.get("/user", authorization, userController.getUsers());
v1.post(
  "/employeeRequest",
  authorization,
  employeeRequestController.createEmployeeRequest()
);
v1.post(
  "/employeeRequestList",
  authorization,
  employeeRequestController.createEmployeeRequestList()
);
v1.get(
  "/getEmployeeRequestList",
  authorization,
  employeeRequestController.getAllRequestCreateByUser()
);

v1.get(
  "/ownerrequest",
  authorization,
  employeeRequestController.getOwnerRequest()
);

v1.get(
  "/employeeRequestApprove",
  authorization,
  employeeRequestController.getRequestListToApprove()
);

v1.get(
  "/employeeRequestManagerChecked",
  authorization,
  employeeRequestController.getRequestListManagerCheck()
);

v1.get(
  "/employeeSecurityCheck",
  authorization,
  employeeRequestController.getRequestListSecurityCheck()
);

v1.get(
  "/allEmployeeSecurityChecked",
  authorization,
  employeeRequestController.getAllRequestListSecurityChecked()
);
v1.get(
  "/employeeRequest/:requestId",
  authorization,
  employeeRequestController.getEmployeeRequestDetail()
);

v1.put(
  "/employeeRequest/:requestId",
  authorization,
  employeeRequestController.updateEmployeeRequest()
);

v1.delete(
  "/employeeRequest/:requestId",
  authorization,
  employeeRequestController.deleteEmployeeRequest()
);

v1.get("/users/all", authorization, userController.getAllUsers());
v1.post("/businessunits", businessUnitController.createBusinessUnitList());
v1.post("/businessunit", businessUnitController.createBusinessUnit());
v1.post("/sections", sectionController.createSectionList());
v1.post("/section", sectionController.createSection());
v1.post("/employee", employeeController.createEmployee());
v1.put("/employee", employeeController.updateEmployee());
v1.post("/employees", employeeController.createEmployeeList());
v1.put("/employees", employeeController.updateEmployeeList());
v1.post("/role", roleController.createRole());
v1.post("/roles", roleController.createRoleList());
v1.post("/user", userController.createUser());
v1.post("/users", userController.createUserList());

module.exports = v1;
