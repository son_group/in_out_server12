const express = require("express");
const authController = require("../../controllers/auth.controller");

const authRouters = express.Router();

authRouters.post("", authController.login());
authRouters.post("", authController.checkPassword());
authRouters.put("", authController.ChangePassword());

module.exports = authRouters;
