const express = require("express");
const authorization = require("../../middleware/authorization");
const userController = require("../../controllers/user.controller");

// path userRouters: /api/v1/users
const userRouters = express.Router();

userRouters.get("", userController.getUsers());
userRouters.post("", userController.createUser());
userRouters.put("/:userid", userController.updateUser());
userRouters.delete("/:userid", userController.deleteUser());

module.exports = userRouters;
