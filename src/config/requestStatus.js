const NEW_STATUS = "Mới";
const APPROVED = "Quản lý đồng ý";
const REJECT = "Quản lý từ chối";
const SECURITY_REJECT = "Bảo vệ từ chối";
const DONE = "Hoàn thành";

module.exports = {
  NEW_STATUS,
  APPROVED,
  REJECT,
  DONE,
  SECURITY_REJECT,
};
