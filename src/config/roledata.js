const NHAN_VIEN = 1;
const SUPPORT = 2;
const QUAN_DOC = 3;
const TRUONG_KHOI = 4;
const PHO_GIAM_DOC = 5;
const TONG_GIAM_DOC = 6;

module.exports = {
  NHAN_VIEN,
  SUPPORT,
  QUAN_DOC,
  TRUONG_KHOI,
  PHO_GIAM_DOC,
  TONG_GIAM_DOC,
};
