module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "mhcadmin",
  DB: "mhc_inout",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
