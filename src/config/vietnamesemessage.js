const EMAIL_PASSWORD_INVALID = "Tên đăng nhập hoặc mật khẩu không đúng!";
const INTERNAL_SERVER = "Lỗi từ phía server";
const EMPLOYEE_NOT_DB = "Mã nhân viên này không tồn tại!";
const INVALID_TOKEN = "Tài khoản đăng nhập không hợp lệ hoặc đã hết hạn!";
const NOT_ROLE = "Tài khoản của bạn không thể tạo cho người khác!";
const REQUEST_NOT_EXIST = "Yêu cầu này đã được xét duyệt";
const DELETE_SUCCESS = "Xoá thành công";
const NOT_IN_SECTION = "Không thể tạo cho người ở bộ phận khác";

module.exports = {
  EMAIL_PASSWORD_INVALID,
  INTERNAL_SERVER,
  EMPLOYEE_NOT_DB,
  INVALID_TOKEN,
  NOT_ROLE,
  REQUEST_NOT_EXIST,
  DELETE_SUCCESS,
  NOT_IN_SECTION,
};
