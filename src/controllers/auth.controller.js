const { response } = require("../helpers/response");
const authService = require("../services/auth.service");

const login = () => {
  return async (req, res, next) => {
    try {
      const credentials = req.body;
      // console.log("go to the auth controller credential: ", credentials);
      const user = await authService.login(credentials);

      res.status(200).json(response(user));
    } catch (error) {
      next(error);
    }
  };
};
const checkPassword = () => {
  return async (req, res, next) => {
    try {
      const credentials = req.body;
      // console.log("go to the auth controller credential: ", credentials);
      const user = await authService.checkPassword(credentials);

      res.status(200).json(response(user));
    } catch (error) {
      next(error);
    }
  };
};
const ChangePassword = () => {
  return async (req, res, next) => {
    try {
      const credentials = req.body;
      // console.log("go to the auth controller credential: ", credentials);
      const user = await authService.ChangePassword(credentials);

      res.status(200).json(response(user));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  login,
  checkPassword,
  ChangePassword,
};
