const { response } = require("../helpers/response");
const employeeService = require("../services/employee.service");

const getEmployeeInfo = () => {
  return async (req, res, next) => {
    try {
      const { employeeId } = req.params;
      const employeeInfo = await employeeService.getEmployeeInfo(employeeId);
      res.status(200).json(response(employeeInfo));
    } catch (error) {
      next(error);
    }
  };
};

const getAllEmployeeSection = () => {
  return async (req, res, next) => {
    try {
      const { userId } = req.params;
    } catch (error) {}
  };
};

const createEmployee = () => {
  return async (req, res, next) => {
    try {
      const employee = req.body;
      console.log("employee request: ", employee);
      const createdEmployee = await employeeService.createEmployee(employee);
      res.status(200).json(response(createdEmployee));
    } catch (error) {
      next(error);
    }
  };
};

const createEmployeeList = () => {
  return async (req, res, next) => {
    try {
      const employeeList = req.body;
      const createdEmployeeList = [];
      for (let i = 0; i < employeeList.length; i++) {
        const createdEmployee = await employeeService.createEmployee(
          employeeList[i]
        );
        createdEmployeeList.push(createdEmployee);
      }
      res.status(200).json(response(createdEmployeeList));
    } catch (error) {
      next(error);
    }
  };
};

const updateEmployee = () => {
  return async (req, res, next) => {
    try {
      // const { employeeId } = req.params;
      const employee = req.body;
      const updatedEmployee = await employeeService.updateEmployee(employee);
      res.status(200).json(response(updatedEmployee));
    } catch (error) {
      next(error);
    }
  };
};

const updateEmployeeList = () => {
  return async (req, res, next) => {
    try {
      const employeeList = req.body;
      const updatedEmployeeList = [];
      for (let i = 0; i < employeeList.length; i++) {
        const updatedEmployee = await employeeService.updateEmployee(
          employeeList[i]
        );
        updatedEmployeeList.push(updatedEmployee);
      }

      res.status(200).json(response(updatedEmployeeList));
    } catch (error) {
      next(error);
    }
  };
};

const deleteEmployee = () => {
  return async (req, res, next) => {
    try {
      const { employeeId } = req.params;
      await employeeService.deleteEmployee(employeeId);
      res.status(200).json(response(true));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  getAllEmployeeSection,
  getEmployeeInfo,
  createEmployeeList,
  createEmployee,
  updateEmployee,
  updateEmployeeList,
  deleteEmployee,
};
