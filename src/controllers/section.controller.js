const { response } = require("../helpers/response");
const sectionService = require("../services/section.service");

const getAllSectionOfBU = () => {
  return async (req, res, next) => {
    try {
      const { sectionId } = req.params;
      const sectionUnits = await sectionService.getAllSectionOfBU(sectionId);
      res.status(200).json(response(sectionUnits));
    } catch (error) {
      next(error);
    }
  };
};

const createSection = () => {
  return async (req, res, next) => {
    try {
      const section = req.body;
      console.log("section: ", section);
      const createdSection = await sectionService.createSection(section);
      res.status(200).json(response(createdSection));
      //   res.status(200).json("OK");
    } catch (error) {
      next(error);
    }
  };
};

const createSectionList = () => {
  return async (req, res, next) => {
    try {
      const sectionList = req.body;
      console.log("sectionList ", sectionList);
      const createdList = [];
      for (let i = 0; i < sectionList.length; i++) {
        const createdSection = await sectionService.createSection(
          sectionList[i]
        );
        createdList.push(createdSection);
        // createdList.push(sectionList[i]);
      }
      res.status(200).json(response(createdList));
    } catch (error) {
      next(error);
    }
  };
};

const updateSection = () => {
  return async (req, res, next) => {
    try {
      const { sectionId } = req.params;
      const section = req.body;
      const updatedSection = await sectionService.updateSection(
        sectionId,
        section
      );
      res.status(200).json(response(updatedSection));
    } catch (error) {
      next(error);
    }
  };
};

const deleteSection = () => {
  return async (req, res, next) => {
    try {
      const { sectionId } = req.params;

      const deletedSection = await sectionService.deleteSection(sectionId);
      res.status(200).json(response("Deleted"));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  getAllSectionOfBU,
  createSectionList,
  createSection,
  updateSection,
  deleteSection,
};
