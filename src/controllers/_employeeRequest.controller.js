const employeeRequestService = require("..");

const getAllEmployeeRequest = () => {
  return async (req, res, next) => {
    try {
      const { createdEmployeeID } = req.body;
      const employeeRequest =
        await employeeRequestService.getAllEmployeeRequest(createdEmployeeID);
      res.status(200).json({ data: employeeRequest });
    } catch (error) {
      next(error);
    }
  };
};

// const createEmployeeRequest = () => {
//   return async (req, res) => {
//     try {
//       const employeeRequest = req.body;
//       const createdEmployeeRequest =
//         await employeeRequestService.createEmployeeRequest(employeeRequest);
//       res.status(200).json({ data: createdEmployeeRequest });
//     } catch (error) {
//       res.status(500).json({ error: error.message });
//     }
//   };
// };

const createEmployeeRequest = () => {
  return async (req, res, next) => {
    try {
      const employeeRequest = req.body;
    } catch (error) {}
  };
};

const updateEmployeeRequest = () => {
  return async (req, res) => {
    try {
      const { requestID } = req.params;
      console.log("request_id ", requestID);
      const employeeRequest = req.body;
      console.log("employeeRequest ", employeeRequest);
      const updatedEmployeeRequest =
        await employeeRequestService.updateEmployeeRequest(
          requestID,
          employeeRequest
        );
      res.status(200).json({ data: updatedEmployeeRequest });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };
};

const deleteEmployeeRequest = () => {
  return async (req, res) => {
    try {
      const { requestID } = req.params;
      await employeeRequestService.deleteEmployeeRequest(requestID);
      res.status(200).json({ data: true });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };
};

module.exports = {
  getAllEmployeeRequest,
  createEmployeeRequest,
  updateEmployeeRequest,
  deleteEmployeeRequest,
};
