const { handleErrors } = require("../helpers/error");
const { response } = require("../helpers/response");
const employeeRequestService = require("../services/employeeRequest.service");

const getAllRequestCreateByUser = () => {
  return async (req, res, next) => {
    try {
      const createdEmployeeId = res.locals.user.employeeId;
      console.log("created EmployeeRequest ", createdEmployeeId);
      const getAllRequestCreated =
        await employeeRequestService.getAllRequestUserCreated(
          createdEmployeeId
        );
      res.status(200).json(response(getAllRequestCreated));
    } catch (error) {
      next(error);
    }
  };
};

const getOwnerRequest = () => {
  return async (req, res, next) => {
    try {
      const employeeId = res.locals.user.employeeId;
      console.log("created EmployeeRequest ", employeeId);
      const getOwnerRequestList = await employeeRequestService.getOwnerRequest(
        employeeId
      );
      res.status(200).json(response(getOwnerRequestList));
    } catch (error) {
      next(error);
    }
  };
};

const getRequestListToApprove = () => {
  return async (req, res, next) => {
    try {
      const user = res.locals.user;
      const sectionId = res.locals.employee.sectionId;
      const getRequestListToApproved =
        await employeeRequestService.getRequestListToApprove(user, sectionId);
      res.status(200).json(response(getRequestListToApproved));
    } catch (error) {
      next(error);
    }
  };
};

const getRequestListManagerCheck = () => {
  return async (req, res, next) => {
    try {
      const user = res.locals.user;

      const getRequestListToApproved =
        await employeeRequestService.getRequestListManagerCheck(user);
      res.status(200).json(response(getRequestListToApproved));
    } catch (error) {
      next(error);
    }
  };
};

const getRequestListSecurityCheck = () => {
  return async (req, res, next) => {
    try {
      const user = res.locals.user;

      const getRequestListSecurityCheck =
        await employeeRequestService.getRequestListSecurityCheck(user);
      res.status(200).json(response(getRequestListSecurityCheck));
    } catch (error) {
      next(error);
    }
  };
};

const getAllRequestListSecurityChecked = () => {
  return async (req, res, next) => {
    try {
      const user = res.locals.user;

      const getAllRequestListSecurityCheck =
        await employeeRequestService.getAllRequestListSecurityChecked(user);
      res.status(200).json(response(getAllRequestListSecurityCheck));
    } catch (error) {
      next(error);
    }
  };
};

const createEmployeeRequest = () => {
  return async (req, res, next) => {
    try {
      const employeeRequest = req.body;
      const sectionId = res.locals.employee.sectionId;
      const user = res.locals.user;
      const createdEmployeeRequest =
        await employeeRequestService.createEmployeeRequest(
          sectionId,
          user,
          employeeRequest
        );
      res.status(200).json(response(createdEmployeeRequest));
    } catch (error) {
      next(error);
    }
  };
};

const createEmployeeRequestList = () => {
  return async (req, res, next) => {
    try {
      const employeeRequestList = req.body;
      const sectionId = res.locals.employee.sectionId;
      const user = res.locals.user;
      const createEmployeeRequestList =
        await employeeRequestService.createEmployeeRequestList(
          sectionId,
          user,
          employeeRequestList
        );

      if (createEmployeeRequestList.length === 0) {
        res.status(200).json(response("sucess"));
      } else {
        res.status(403).json(createEmployeeRequestList);
      }
    } catch (error) {
      next(error);
    }
  };
};

const updateEmployeeRequest = () => {
  return async (req, res, next) => {
    try {
      const { requestId } = req.params;
      const employeeRequest = req.body;
      const updatedEmployeeRequest =
        await employeeRequestService.updateEmployeeRequest(
          requestId,
          employeeRequest
        );
      res.status(200).json(response(updatedEmployeeRequest));
    } catch (error) {
      next(error);
    }
  };
};

const getEmployeeRequestDetail = () => {
  return async (req, res, next) => {
    try {
      console.log(req.params);
      const { requestId } = req.params;
      const employeeRequestDetail =
        await employeeRequestService.getEmployeeRequestDetail(requestId);
      res.status(200).json(response(employeeRequestDetail));
    } catch (error) {
      next(error);
    }
  };
};

const deleteEmployeeRequest = () => {
  return async (req, res, next) => {
    try {
      const { requestId } = req.params;
      await employeeRequestService.deleteEmployeeRequest(requestId);
      res.status(200).json(response(true));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  getAllRequestCreateByUser,
  getOwnerRequest,
  getRequestListToApprove,
  getRequestListManagerCheck,
  getRequestListSecurityCheck,
  getAllRequestListSecurityChecked,
  getEmployeeRequestDetail,
  createEmployeeRequest,
  createEmployeeRequestList,
  updateEmployeeRequest,
  deleteEmployeeRequest,
};
