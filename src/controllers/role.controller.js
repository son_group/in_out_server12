const { response } = require("../helpers/response");
const roleService = require("../services/role.service");

const getAllRoles = () => {
  return async (req, res, next) => {
    try {
      const roles = await roleService.getAllRole();
      res.status(200).json(response(roles));
    } catch (error) {
      next(error);
    }
  };
};

const createRole = () => {
  return async (req, res, next) => {
    try {
      const role = req.body;
      const createdRole = await roleService.createRole(role);
      res.status(200).json(response(createdRole));
    } catch (error) {
      next(error);
    }
  };
};

const createRoleList = () => {
  return async (req, res, next) => {
    try {
      const role = req.body;
      const roleListCreated = [];
      for (let i = 0; i < role.length; i++) {
        const createdRole = await roleService.createRole(role[i]);
        roleListCreated.push(createdRole);
      }

      res.status(200).json(response(roleListCreated));
    } catch (error) {
      next(error);
    }
  };
};

const updateRole = () => {
  return async (req, res, next) => {
    try {
      const { roleId } = req.params;
      const roleName = req.body;
      const updatedRole = await roleService.updateRole(roleId, roleName);
      res.status(200).json(response(updatedRole));
    } catch (error) {
      next(error);
    }
  };
};

const deleteRole = () => {
  return async (req, res, next) => {
    try {
      const { roleId } = req.params;

      const deletedRole = await roleService.deleteRole(roleId);
      res.status(200).json(response(deletedRole));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  getAllRoles,
  createRole,
  createRoleList,
  updateRole,
  deleteRole,
};
