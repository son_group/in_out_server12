const { response } = require("../helpers/response");
const userService = require("../services/user.services");

const getUsers = () => {
  return async (req, res, next) => {
    try {
      const userId = res.locals.user.userId;
      // console.log("user Id ", userId);
      const users = await userService.getUserInfo(userId);
      res.status(200).json(response(users));
    } catch (error) {
      next(error);
    }
  };
};

const getAllUsers = () => {
  return async (req, res, next) => {
    try {
      console.log("get all users");
      const users = await userService.getAllUser({ limit: 3 });
      res.status(200).json(response(users));
    } catch (error) {
      next(error);
    }
  };
};

const createUser = () => {
  return async (req, res, next) => {
    try {
      const user = req.body;
      console.log("user data from body: ", user);
      const createdUser = await userService.createUser(user);
      res.status(200).json(response(createdUser));
    } catch (error) {
      next(error);
    }
  };
};

const createUserList = () => {
  return async (req, res, next) => {
    try {
      const userlist = req.body;

      const createdUserList = [];
      for (let i = 0; i < userlist.length; i++) {
        console.log("user create ", userlist[i]);
        const createdUser = await userService.createUser(userlist[i]);
        createdUserList.push(createdUser);
      }
      res.status(200).json(response(createdUserList));
    } catch (error) {
      next(error);
    }
  };
};

const updateUser = () => {
  return async (req, res, next) => {
    try {
      const { userid } = req.params;
      const user = req.body;
      const updatedUser = await userService.updateUser(userid, user);
      res.status(200).json(response(updatedUser));
    } catch (error) {
      next(error);
    }
  };
};

const deleteUser = () => {
  return async (req, res, next) => {
    try {
      const { userid } = req.params;
      await userService.deleteUser(userid);
      res.status(200).json(response(true));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  getUsers,
  getAllUsers,
  createUser,
  createUserList,
  updateUser,
  deleteUser,
};
