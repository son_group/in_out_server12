const { response } = require("../helpers/response");
const businessUnitService = require("../services/businessUnit.service");

const getAllBusinessUnit = () => {
  return async (req, res, next) => {
    try {
      const businessUnits = await businessUnitService.getAllBusinessUnit();
      res.status(200).json(response(businessUnits));
    } catch (error) {
      next(error);
    }
  };
};

const createBusinessUnit = () => {
  return async (req, res, next) => {
    try {
      const businessUnit = req.body;

      const createdBusinessUnit = await businessUnitService.createBusinessUnit(
        businessUnit
      );
      res.status(200).json(response(createdBusinessUnit));
    } catch (error) {
      next(error);
    }
  };
};

const createBusinessUnitList = () => {
  return async (req, res, next) => {
    try {
      const businessUnitList = req.body;
      const createdList = [];
      for (let i = 0; i < businessUnitList.length; i++) {
        const createdBusinessUnit =
          await businessUnitService.createBusinessUnit(businessUnitList[i]);
        createdList.push(createdBusinessUnit);
      }
      res.status(200).json(response(createdList));
    } catch (error) {
      next(error);
    }
  };
};

const updateBusiness = () => {
  return async (req, res, next) => {
    try {
      const { businessId } = req.params;
      const businessName = req.body;
      const updatedBusinessUnit = await businessUnitService.updateBusinessUnit(
        businessId,
        businessName
      );
      res.status(200).json(res(updatedBusinessUnit));
    } catch (error) {
      next(error);
    }
  };
};

const deleteBusinessUnit = () => {
  return async (req, res, next) => {
    try {
      const { businessId } = req.params;

      const deletedBusinessUnit = await businessUnitService.deleteBusinessUnit(
        businessId
      );
      res.status(200).json(res(deletedBusinessUnit));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  getAllBusinessUnit,
  createBusinessUnit,
  createBusinessUnitList,
  updateBusiness,
  deleteBusinessUnit,
};
