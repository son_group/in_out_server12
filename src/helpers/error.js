const { INTERNAL_SERVER } = require("../config/vietnamesemessage");

class AppError extends Error {
  constructor(statusCode, message) {
    super(message);
    this.statusCode = statusCode;
  }
}

const handleErrors = (err, req, res, next) => {
  if (!(err instanceof AppError)) {
    err = new AppError(500, INTERNAL_SERVER);
  }

  const { message, statusCode } = err;
  res.status(statusCode).json({
    status: "Error",
    message: message,
  });
  next();
};

module.exports = {
  AppError,
  handleErrors,
};
