const jwt = require("jsonwebtoken");
const SECRET_CODE = require("../config/auth.secret");
const EXPIRES_IN = 60 * 60 * 8;

const generateToken = (payload) => {
  console.log("before token");
  const token = jwt.sign(
    {
      userId: payload.userId,
    },
    SECRET_CODE,
    {
      expiresIn: EXPIRES_IN,
    }
  );

  console.log("token ", token);

  return {
    userId: payload.userId,
    employeeId: payload.employeeId,
    roleId: payload.roleId,
    token,
    expiresIn: EXPIRES_IN,
    sectionId: payload.Employee.sectionId,
  };
};

module.exports = {
  generateToken,
};
