const express = require("express");
const cors = require("cors");
const { AppError, handleErrors } = require("./helpers/error");
const { sequelize } = require("./models");

const v1 = require("./routers/v1");
const app = express();
app.use(cors());
app.use(express.json());
// sequelize.sync({ alert: true });

app.use("/api/v1", v1);

// app.get("/error", (req, res) => {
//   throw new AppError(500, "Internal Server");
// });

app.use(handleErrors);

app.listen("4000");
