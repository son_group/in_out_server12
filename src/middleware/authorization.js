const jwt = require("jsonwebtoken");
const SECRET_CODE = require("../config/auth.secret");
const { INVALID_TOKEN } = require("../config/vietnamesemessage");
const { AppError } = require("../helpers/error");
const db = require("../models");

const extractTokenFromHeader = (headers) => {
  const bearerToken = headers.authorization; // Bearer abcxyz
  const parts = bearerToken.split(" ");

  if (parts.length !== 2 || parts[0] !== "Bearer" || !parts[1].trim()) {
    throw new AppError(401, INVALID_TOKEN);
  }

  return parts[1];
};

const authorization = async (req, res, next) => {
  try {
    const token = extractTokenFromHeader(req.headers);
    // console.log("token from front end", token);
    const payload = jwt.verify(token, SECRET_CODE);

    const user = await db.user.findByPk(payload.userId, {
      include: ["Role", "Employee"],
    });
    if (!user) {
      throw new AppError(401, INVALID_TOKEN);
    }
    res.locals.user = user.dataValues;
    res.locals.employee = user.dataValues.Employee.dataValues;
    // console.log("pass authen check");
    next();
  } catch (error) {
    if (error instanceof jwt.JsonWebTokenError) {
      next(new AppError(401, INVALID_TOKEN));
    }
    next(error);
  }
};

module.exports = authorization;
