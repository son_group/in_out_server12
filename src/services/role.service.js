const { AppError } = require("../helpers/error");
const db = require("../models");
const Role = db.role;

const getRoleInfo = async (roleId) => {
  try {
    const role = await Role.findOne({
      where: {
        roleId: roleId,
      },
    });
    return role;
  } catch (error) {
    throw error;
  }
};

const getAllRole = async () => {
  try {
    const role = await Role.findAll();
    return role;
  } catch (error) {
    throw error;
  }
};

const createRole = async (roledata) => {
  try {
    const role = await Role.findOne({
      where: {
        roleId: roledata.roleId,
      },
    });
    if (role) {
      throw new AppError(400, "Role exits on database!");
    }

    const createdRole = Role.create(roledata);
    return createdRole;
  } catch (error) {
    throw error;
  }
};

const updateRole = async (roleId, roleName) => {
  try {
    const role = await Role.findOne({
      where: {
        roleId: roleId,
      },
    });
    if (!role) {
      throw new AppError(400, "Role does not exist!");
    }

    const updatedRole = await Role.update(
      {
        roleName: roleName,
      },
      {
        where: {
          roleId: roleId,
        },
      }
    );
    return updatedRole;
  } catch (error) {
    throw error;
  }
};

const deleteRole = async (roleId) => {
  try {
    const role = await Role.findOne({
      where: {
        roleId: roleId,
      },
    });
    if (!role) {
      throw new AppError(400, "Role does not exist!");
    }

    await Role.destroy({
      where: {
        roleId: roleId,
      },
    });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getRoleInfo,
  getAllRole,
  createRole,
  updateRole,
  deleteRole,
};
