const { AppError } = require("../helpers/error");
const db = require("../models");
const Section = db.section;
const BusinessUnit = db.businessUnit;

const getSectionInfo = async (sectionId) => {
  try {
    const section = await Section.findOne({
      where: {
        sectionId: sectionId,
      },
    });
    return section;
  } catch (error) {
    throw error;
  }
};

const getAllSectionOfBU = async (sectionId) => {
  try {
    const section = await Section.findOne({
      where: {
        sectionId: sectionId,
      },
    });

    const sections = await Section.findAll({
      where: {
        businessId: section.businessId,
      },
    });
    return sections;
  } catch (error) {
    throw error;
  }
};

const createSection = async (sectiondata) => {
  try {
    console.log("section data", sectiondata);
    const business = await BusinessUnit.findOne({
      where: {
        businessId: sectiondata.businessId,
      },
    });
    if (!business) {
      throw new AppError(400, "Business Unit does not exist");
    }

    const section = await Section.findOne({
      where: {
        sectionId: sectiondata.sectionId,
      },
    });
    if (section) {
      throw new AppError(400, "Section exits on database!");
    }

    const createdSection = Section.create(sectiondata);
    return createdSection;
  } catch (error) {
    throw error;
  }
};

const updateSection = async (sectionId, sectiondata) => {
  try {
    const section = await Section.findOne({
      where: {
        sectionId: sectionId,
      },
    });
    if (!section) {
      throw new AppError(400, "Section does not exist!");
    }

    const businessUnit = await BusinessUnit.findOne({
      where: {
        businessId: section.businessId,
      },
    });

    if (!businessUnit) {
      throw new AppError(400, "Business Unit does not exist!");
    }

    const updatedSection = await Section.update(
      {
        sectionName: sectiondata.sectionName,
        businessId: sectiondata.businessId,
      },
      {
        where: {
          sectionId: sectiondata.sectionId,
        },
      }
    );
    return updatedSection;
  } catch (error) {
    throw error;
  }
};

const deleteSection = async (sectionId) => {
  try {
    const section = await Section.findOne({
      where: {
        sectionId: sectionId,
      },
    });
    if (!section) {
      throw new AppError(400, "Section does not exist!");
    }

    await Section.destroy({
      where: {
        sectionId: sectionId,
      },
    });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getSectionInfo,
  getAllSectionOfBU,
  createSection,
  updateSection,
  deleteSection,
};
