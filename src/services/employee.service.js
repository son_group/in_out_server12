const { AppError } = require("../helpers/error");
const db = require("../models");
const Employee = db.employee;
const Section = db.section;

const getEmployeeInfo = async (employeeId) => {
  try {
    const employeeInfo = await Employee.findOne({
      where: {
        employeeId: employeeId,
      },
    });
    return employeeInfo;
  } catch (error) {
    throw error;
  }
};

const getEmployeesSection = async (employeeId) => {
  try {
    const employee = await Employee.findOne({
      where: {
        employeeId: employeeId,
      },
    });
    const employeesSection = await Employee.findAll({
      where: {
        sectionId: employee.sectionId,
      },
    });
    return employeesSection;
  } catch (error) {
    throw error;
  }
};

const getAllEmployees = async () => {
  try {
    const employees = await Employee.findAll();
    return employees;
  } catch (error) {
    throw error;
  }
};

const createEmployee = async (employeedata) => {
  try {
    const employee = await Employee.findOne({
      where: {
        employeeId: employeedata.employeeId,
      },
    });
    if (employee) {
      throw new AppError(400, "Employee exits on database!");
    }

    const section = await Section.findOne({
      where: {
        sectionId: employeedata.sectionId,
      },
    });

    if (!section) {
      throw new AppError(400, "Section does not exist!");
    }

    const createdEmployee = Employee.create(employeedata);
    return createdEmployee;
  } catch (error) {
    throw error;
  }
};

const updateEmployee = async (employeedata) => {
  try {
    console.log(" Go to employee update service .....");
    const employee = await Employee.findOne({
      where: {
        employeeId: employeedata.employeeId,
      },
    });
    if (!employee) {
      throw new AppError(400, "Employee does not exist!");
    }

    const section = await Section.findOne({
      where: {
        sectionId: employee.sectionId,
      },
    });

    if (!section) {
      throw new AppError(400, "Section does not exist!");
    }

    const updatedEmployee = Employee.update(
      {
        employeeName: employeedata.employeeName,
        dob: employeedata.dob,
        position: employeedata.position,
        sectionId: employeedata.sectionId,
      },
      {
        where: {
          employeeId: employeedata.employeeId,
        },
      }
    );
    return updatedEmployee;
  } catch (error) {
    throw error;
  }
};

const deleteEmployee = async (deleteEmployeeId) => {
  try {
    const employee = Employee.findOne({
      where: {
        employeeId: deleteEmployeeId,
      },
    });

    if (!employee) {
      throw new AppError(400, "Employee does not exist!");
    }

    await Employee.destroy({
      where: {
        employeeId: deleteEmployeeId,
      },
    });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getEmployeeInfo,
  getEmployeesSection,
  getAllEmployees,
  createEmployee,
  updateEmployee,
  deleteEmployee,
};
