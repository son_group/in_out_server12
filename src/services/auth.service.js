const bcrypt = require("bcrypt");
const { EMAIL_PASSWORD_INVALID } = require("../config/vietnamesemessage");
const { AppError } = require("../helpers/error");
const { generateToken } = require("../helpers/jwt");
const db = require("../models");

const login = async (creadentials) => {
  try {
    const { userId, password } = creadentials;
    const user = await db.user.findOne({
      where: { userId },
      attributes: { include: ["password"] },
      include: "Employee",
    });
    if (!user) {
      throw new AppError(400, EMAIL_PASSWORD_INVALID);
    }
    const isMatched = bcrypt.compareSync(password, user.password);

    if (!isMatched) {
      throw new AppError(400, EMAIL_PASSWORD_INVALID);
    }

    console.log("user ", user);
    const usertoken = generateToken(user);
    console.log("user token ", usertoken);

    return generateToken(user);
  } catch (error) {
    throw error;
  }
};
const checkPassword = async (dataCheck) => {
  try {
    const { userId, password } = dataCheck;
    const user = await db.user.findOne({
      where: { userId },
      attributes: { include: ["password"] },
      include: "Employee",
    });
    if (!user) {
      return false;
    }
    const isMatched = bcrypt.compareSync(password, user.password);

    if (!isMatched) {
      return false;
    }
    return true;
  } catch (error) {
    return false;
  }
};
const ChangePassword = async (data) => {
  try {
    const { userId, password } = data;
    const user = await db.user.findOne({
      where: { userId },
      attributes: { include: ["password"] },
      include: "Employee",
    });
    if (!user) {
      throw new AppError(400, EMAIL_PASSWORD_INVALID);
    }
    const salt = bcrypt.genSaltSync();
    const hashedPassword = bcrypt.hashSync(password, salt);
    const isUpdated = await db.user.update(
      { password: hashedPassword },
      { where: { userId } }
    );
    return true;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  login,
  ChangePassword,
  checkPassword,
};
