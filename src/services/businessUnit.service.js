const { AppError } = require("../helpers/error");
const db = require("../models");
const BusinessUnit = db.businessUnit;

const getBusinessInfo = async (businessId) => {
  try {
    const businessUnit = await BusinessUnit.findOne({
      where: {
        businessId: businessId,
      },
    });
    return businessUnit;
  } catch (error) {
    throw error;
  }
};

const getAllBusinessUnit = async () => {
  try {
    const businessUnits = await BusinessUnit.findAll();
    return businessUnits;
  } catch (error) {
    throw error;
  }
};

const createBusinessUnit = async (businessUnitdata) => {
  try {
    const businessUnit = await BusinessUnit.findOne({
      where: {
        businessId: businessUnitdata.businessId,
      },
    });
    if (businessUnit) {
      throw new AppError(400, "Business Unit exits on database!");
    }

    const createdBusinessUnit = BusinessUnit.create(businessUnitdata);
    return createdBusinessUnit;
  } catch (error) {
    throw error;
  }
};

const updateBusinessUnit = async (businessId, busniessName) => {
  try {
    const businessUnit = await BusinessUnit.findOne({
      where: {
        businessId: businessId,
      },
    });
    if (!businessUnit) {
      throw new AppError(400, "Business Unit does not exist!");
    }

    const updatedBusinessUnit = await BusinessUnit.update(
      {
        businessName: busniessName,
      },
      {
        where: {
          businessId: businessId,
        },
      }
    );
    return updatedBusinessUnit;
  } catch (error) {
    throw error;
  }
};

const deleteBusinessUnit = async (businessId) => {
  try {
    const businessUnit = await BusinessUnit.findOne({
      where: {
        businessId: businessId,
      },
    });
    if (!businessUnit) {
      throw new AppError(400, "Business Unit does not exist!");
    }

    await BusinessUnit.destroy({
      where: {
        businessId: businessId,
      },
    });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getBusinessInfo,
  getAllBusinessUnit,
  createBusinessUnit,
  updateBusinessUnit,
  deleteBusinessUnit,
};
