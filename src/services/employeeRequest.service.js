const moment = require("moment/moment");
const { Op, Sequelize, DATE } = require("sequelize");
const {
  NEW_STATUS,
  APPROVED,
  DONE,
  SECURITY_REJECT,
} = require("../config/requestStatus");
const {
  EMPLOYEE_NOT_DB,
  NOT_IN_SECTION,
  REQUEST_NOT_EXIST,
  NOT_ROLE,
} = require("../config/vietnamesemessage");
const { AppError } = require("../helpers/error");
const db = require("../models");
const Employee = db.employee;
const EmployeeRequest = db.employeeRequest;
const Role = db.role;
const Section = db.section;
const BusinessUnit = db.businessUnit;

const getAllEmployeeRequest = async (user) => {
  try {
    const employeerequest = EmployeeRequest.findAll({
      where: {
        employeeId: user.employeeId,
      },
    });
    return employeerequest;
  } catch (error) {
    throw error;
  }
};

const getAllRequestUserCreated = async (employeeId) => {
  try {
    console.log("go to getAll");
    const employeeRequestCreated = await EmployeeRequest.findAll({
      where: {
        createdEmployeeId: employeeId,
        requestStatus: NEW_STATUS,
      },
      include: "Employee",
    });

    return employeeRequestCreated;
  } catch (error) {
    throw error;
  }
};

const getOwnerRequest = async (employeeId) => {
  try {
    console.log("go to getAll");
    const employeeRequestCreated = await EmployeeRequest.findAll({
      where: {
        employeeId: employeeId,
      },
      include: [
        {
          model: Employee,
          where: {
            employeeId: employeeId,
          },
        },
      ],
    });

    return employeeRequestCreated;
  } catch (error) {
    throw error;
  }
};

const getApproveAsRole = async (user, sectionId) => {
  try {
    if (user.roleId === 3) {
      const employeeRequest = await EmployeeRequest.findAll({
        where: {
          requestStatus: NEW_STATUS,
          employeeId: {
            [Op.ne]: user.Employee.employeeId,
          },
        },
        include: [
          {
            model: Employee,
            where: {
              sectionId: sectionId,
              [Op.not]: [{ position: ["TK", "PTGD", "GD"] }],
            },
          },
        ],
      });
      return employeeRequest;
    }
    if (user.roleId === 4) {
      const section = await Section.findOne({
        where: {
          sectionId: sectionId,
        },
      });
      const sectionList = await Section.findAll({
        where: {
          businessId: section.businessId,
        },
        attributes: ["sectionId"],
        raw: true,
      });

      const list = sectionList.map((section) => {
        return section.sectionId;
      });

      const employeeRequest = await EmployeeRequest.findAll({
        where: {
          requestStatus: NEW_STATUS,
          employeeId: {
            [Op.ne]: user.Employee.employeeId,
          },
        },
        include: [
          {
            model: Employee,
            where: {
              // position: "TBP",
              sectionId: list,
              [Op.not]: [{ position: ["PTGD", "GD"] }],
            },
          },
        ],
        order: [["requestDate", "ASC"]],
      });
      return employeeRequest;
    }

    if (user.roleId === 5) {
      const employeeRequest = await EmployeeRequest.findAll({
        where: {
          requestStatus: NEW_STATUS,
          employeeId: {
            [Op.ne]: user.Employee.employeeId,
          },
        },
        include: [
          {
            model: Employee,
            where: {
              position: ["TK", "TBP"],
            },
          },
        ],
        order: [["requestDate", "ASC"]],
      });
      return employeeRequest;
    }

    if (user.roleId === 6) {
      const employeeRequest = await EmployeeRequest.findAll({
        where: {
          requestStatus: NEW_STATUS,
          employeeId: {
            [Op.ne]: user.Employee.employeeId,
          },
        },
        include: [
          {
            model: Employee,
            where: {
              position: ["PTGD", "TK"],
            },
          },
        ],
        order: [["requestDate", "ASC"]],
      });
      return employeeRequest;
    }
  } catch (error) {
    next(error);
  }
};

const getRequestListToApprove = async (userdata, sectionId) => {
  try {
    console.log("go to service");
    return getApproveAsRole(userdata, sectionId);
  } catch (error) {
    throw error;
  }
};

const getRequestListManagerCheck = async (userdata) => {
  try {
    const employeeRequest = await EmployeeRequest.findAll({
      where: {
        managerEmployeeId: userdata.employeeId,
      },
      include: [
        {
          model: Employee,
        },
      ],
      order: [["requestDate", "ASC"]],
    });
    return employeeRequest;
  } catch (error) {
    throw error;
  }
};

const getRequestListSecurityCheck = async (userdata) => {
  try {
    const today = moment().format("YYYY-MM-DD");
    const employeeRequest = await EmployeeRequest.findAll({
      where: {
        requestStatus: APPROVED,
        requestDate: today,
      },
      include: [
        {
          model: Employee,
        },
      ],
    });
    return employeeRequest;
  } catch (error) {
    throw error;
  }
};

const getAllRequestListSecurityChecked = async (userdata) => {
  try {
    const employeeRequest = await EmployeeRequest.findAll({
      where: {
        requestStatus: [DONE, SECURITY_REJECT],
      },
      include: [
        {
          model: Employee,
        },
      ],
    });
    return employeeRequest;
  } catch (error) {
    throw error;
  }
};

const createEmployeeRequest = async (sectionId, user, requestData) => {
  try {
    console.log("employeeRequest ", requestData);
    const employee = await Employee.findOne({
      where: {
        employeeId: requestData.employeeId,
      },
    });
    if (!employee) {
      throw new AppError(400, EMPLOYEE_NOT_DB);
    }

    if (employee.sectionId !== sectionId) {
      throw new AppError(403, NOT_IN_SECTION);
    }

    if (user.roleId !== 2 && user.employeeId !== requestData.employeeId) {
      throw new AppError(403, NOT_ROLE);
    }

    console.log("go to this, employeeTime ", requestData);
    const createdEmployeeRequest = await EmployeeRequest.create({
      employeeId: requestData.employeeId,
      createdEmployeeId: requestData.createdEmployeeId,
      requestDate: requestData.requestDate,
      requestTime: requestData.requestTime,
      requestReason: requestData.requestReason,
      requestType: requestData.requestType,
      requestStatus: requestData.requestStatus,
      requestLocation: requestData.requestLocation,
    });

    // const createdEmployeeRequest = await EmployeeRequest.create(requestData);
    return createdEmployeeRequest;
  } catch (error) {
    throw error;
  }
};

const createEmployeeRequestList = async (sectionId, user, requestList) => {
  try {
    console.log(" request List in service ", requestList);
    let validRequest = [];
    let invalidRequest = [];
    for (const request of requestList) {
      console.log(" request ", request);
      const employee = await Employee.findOne({
        where: {
          employeeId: request.employeeId,
        },
      });

      if (!employee) {
        invalidRequest.push({
          employeeId: employee.employeeId,
          reason: EMPLOYEE_NOT_DB,
        });
      } else {
        if (employee.sectionId !== sectionId) {
          invalidRequest.push({
            employeeId: employee.employeeId,
            reason: NOT_IN_SECTION,
          });
        } else {
          if (user.roleId === 2 || user.employeeId === request.employeeId) {
            validRequest.push(request);
          } else {
            invalidRequest.push({
              employeeId: employee.employeeId,
              reason: NOT_ROLE,
            });
          }
        }
      }
    }
    console.log(
      "go to this, requestList ",
      validRequest,
      "invalid ",
      invalidRequest
    );
    for (requestData of validRequest) {
      const createdEmployeeRequest = await EmployeeRequest.create({
        employeeId: requestData.employeeId,
        createdEmployeeId: requestData.createdEmployeeId,
        requestDate: requestData.requestDate,
        requestTime: requestData.requestTime,
        requestReason: requestData.requestReason,
        requestType: requestData.requestType,
        requestStatus: requestData.requestStatus,
        requestLocation: requestData.requestLocation,
      });
    }

    return invalidRequest;
  } catch (error) {
    throw error;
  }
};

const editEmployeeRequest = async (employeeRequest) => {
  try {
    const employeeRequestDB = await EmployeeRequest.findOne({
      where: {
        requestID: employeeRequest.requestId,
      },
    });
    if (!employeeRequestDB) {
      throw new Error("requestID is not exit");
    }
    const editedEmployeeRequest = await EmployeeRequest.update(
      {
        employeeID: employeeRequest.employeeID,
        requestDate: employeeRequest.requestDate,
        requestTimeIn: employeeRequest.requestTimeIn,
        requestTimeOut: employeeRequest.requestTimeOut,
        requestReason: employeeRequest.requestReason,
        requestType: employeeRequest.requestType,
        requestStatus: employeeRequest.requestStatus,
      },
      {
        where: {
          requestID: employeeRequest.requestId,
        },
      }
    );
    return editedEmployeeRequest;
  } catch (error) {
    throw error;
  }
};

const updateEmployeeRequest = async (requestId, employeeRequestUpdate) => {
  try {
    const employeeRequest = await EmployeeRequest.findOne({
      where: {
        requestId: requestId,
      },
    });
    if (!employeeRequest) {
      throw new AppError(400, "Request does not exist");
    }

    const updatedEmployeeRequest = await EmployeeRequest.update(
      {
        requestStatus: employeeRequestUpdate.requestStatus,
        rejectReason: employeeRequestUpdate.rejectReason,
        managerEmployeeId: employeeRequestUpdate.managerEmployeeId,
        securityEmployeeId: employeeRequestUpdate.securityEmployeeId,
      },
      {
        where: {
          requestId: requestId,
        },
      }
    );

    return updatedEmployeeRequest;
  } catch (error) {
    throw error;
  }
};

const getEmployeeRequestDetail = async (request_id) => {
  try {
    const employeeRequestDB = await EmployeeRequest.findOne({
      where: {
        requestId: request_id,
      },
    });
    return employeeRequestDB;
  } catch (error) {
    console.log(error);
  }
};

const deleteEmployeeRequest = async (request_id) => {
  try {
    const employeeRequestDB = await EmployeeRequest.findOne({
      where: {
        requestId: request_id,
        requestStatus: NEW_STATUS,
      },
    });

    if (!employeeRequestDB) {
      throw new Error(400, REQUEST_NOT_EXIST);
    }

    await EmployeeRequest.destroy({
      where: { requestId: request_id, requestStatus: NEW_STATUS },
    });
    return true;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getAllEmployeeRequest,
  getAllRequestUserCreated,
  getOwnerRequest,
  getRequestListToApprove,
  getRequestListManagerCheck,
  getRequestListSecurityCheck,
  getAllRequestListSecurityChecked,
  getEmployeeRequestDetail,
  createEmployeeRequest,
  createEmployeeRequestList,
  editEmployeeRequest,
  updateEmployeeRequest,
  deleteEmployeeRequest,
};
