const { AppError } = require("../helpers/error");
const bcrypt = require("bcrypt");
const db = require("../models");
const { salt } = require("../config/salt");
const User = db.user;

const getUserInfo = async (userIdreq) => {
  try {
    const user = await User.findOne({
      where: {
        userId: userIdreq,
      },
      attributes: {
        exclude: ["password"],
      },
      include: ["Role", "Employee"],
    });
    // console.log("users in database ", user);
    // console.log(user.__proto__);
    return user;
  } catch (error) {
    throw error;
  }
};

const getAllUser = async () => {
  try {
    console.log("get all user service");
    const alluser = await User.findAll({ limit: 3 });
    console.log("all user ", alluser);
    return alluser;
  } catch (error) {
    throw error;
  }
};

const getUserOfSection = async (user) => {
  try {
    const allUserSection = await User.findAll({
      where: {
        sectionId: user.SectionId,
      },
    });
    return allUserSection;
  } catch (error) {
    throw error;
  }
};

const createUser = async (userdata) => {
  try {
    const user = await User.findOne({
      where: {
        userId: userdata.userId,
      },
    });
    if (user) {
      throw new AppError(400, "User already existed on database!");
    }

    const createdUser = await User.create({
      userId: userdata.userId,
      password: bcrypt.hashSync(userdata.password, salt),
      employeeId: userdata.employeeId,
      roleId: userdata.roleId,
    });
    return createdUser;
  } catch (error) {
    throw error;
  }
};

const updateUserPassword = async (userId, oldPassword, newPassword) => {};

const updateUser = async (userUpdate) => {
  try {
    const user = await User.findOne({
      where: {
        userId: userUpdate.userId,
      },
    });
    if (!user) {
      throw new AppError(400, "User does not exist!");
    }

    const updatedUser = await User.update(
      {
        roleId: userUpdate.roledId,
        sectionId: userUpdate.sectionId,
      },
      {
        where: {
          userId: userUpdate.userId,
        },
      }
    );
    return updatedUser;
  } catch (error) {
    throw error;
  }
};

const deleteUser = async (userId) => {
  try {
    const user = await User.findOne({
      where: {
        userId: userId,
      },
    });
    if (!user) {
      throw new AppError(400, "User does not exist");
    }

    await User.destroy({
      where: {
        userId: userId,
      },
    });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getUserInfo,
  getAllUser,
  getUserOfSection,
  createUser,
  updateUser,
  updateUserPassword,
  deleteUser,
};
