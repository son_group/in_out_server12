module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define(
    "User",
    {
      userId: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: "user_id",
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      employeeId: {
        type: Sequelize.STRING,
        field: "employee_id",
      },
      roleId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        field: "role_id",
      },
    },
    {
      tableName: "users",
    }
  );
  return User;
};
