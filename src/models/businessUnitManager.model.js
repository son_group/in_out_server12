module.exports = (sequelize, Sequelize) => {
  const BusinessUnitManager = sequelize.define(
    "BusinessUnitManager",
    {
      employeeId: {
        type: Sequelize.STRING,
        field: "employee_id",
      },
      businessId: {
        type: Sequelize.STRING,
        field: "business_id",
      },
    },
    {
      tableName: "business_unit_manager",
    }
  );
  return BusinessUnitManager;
};
