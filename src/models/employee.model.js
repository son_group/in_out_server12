module.exports = (sequelize, Sequelize) => {
  const Employee = sequelize.define(
    "Employee",
    {
      employeeId: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: "employee_id",
      },
      employeeName: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "employee_name",
      },
      dob: {
        type: Sequelize.DATEONLY,
      },
      position: {
        type: Sequelize.STRING,
      },
      sectionId: {
        type: Sequelize.STRING,
        field: "section_id",
      },
      startWorkingDate: {
        type: Sequelize.DATEONLY,
        field: "start_working_date",
      },
    },
    {
      tableName: "employees",
    }
  );

  return Employee;
};
