module.exports = (sequelize, Sequelize) => {
  const SectionManager = sequelize.define(
    "SectionManager",
    {
      employeeId: {
        type: Sequelize.STRING,
        field: "employee_id",
      },
      sectionId: {
        type: Sequelize.STRING,
        field: "section_id",
      },
    },
    {
      tableName: "section_manager",
    }
  );

  return SectionManager;
};
