module.exports = (sequelize, Sequelize) => {
  const Section = sequelize.define(
    "Section",
    {
      sectionId: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: "section_id",
      },
      sectionName: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "section_name",
      },
      businessId: {
        type: Sequelize.STRING,
        // allowNull: false,
        field: "business_id",
      },
    },
    {
      tableName: "sections",
    }
  );
  return Section;
};
