module.exports = (sequelize, Sequelize) => {
  const BusinessUnit = sequelize.define(
    "BusinessUnit",
    {
      businessId: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: "business_id",
      },
      businessName: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "business_name",
      },
    },
    {
      tableName: "business_units",
    }
  );

  return BusinessUnit;
};
