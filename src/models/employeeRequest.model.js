module.exports = (sequelize, Sequelize) => {
  const EmployeeRequest = sequelize.define(
    "EmployeeRequest",
    {
      requestId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: "request_id",
      },
      employeeId: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "employee_id",
      },
      createdEmployeeId: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "created_employee_id",
      },
      requestDate: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        field: "request_date",
      },
      requestTime: {
        type: Sequelize.TIME,
        allowNull: false,
        field: "request_time",
      },
      requestReason: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "request_reason",
      },
      requestType: {
        type: Sequelize.STRING(100),
        allowNull: false,
        field: "request_type",
      },
      requestStatus: {
        type: Sequelize.STRING(20),
        allowNull: false,
        field: "request_status",
      },
      requestLocation: {
        type: Sequelize.STRING(20),
        allowNull: false,
        field: "request_location",
      },
      managerEmployeeId: {
        type: Sequelize.STRING,
        field: "manager_employee_id",
      },
      securityEmployeeId: {
        type: Sequelize.STRING,
        field: "security_employee_id",
      },
      rejectReason: {
        type: Sequelize.TEXT,
        field: "reject_reason",
      },
    },
    {
      tableName: "employee_requests",
    }
  );
  return EmployeeRequest;
};
