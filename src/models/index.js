const config = require("../config/db.config");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("../models/user.model")(sequelize, Sequelize);
db.employee = require("../models/employee.model")(sequelize, Sequelize);
db.section = require("../models/section.model")(sequelize, Sequelize);
db.businessUnit = require("../models/businessUnit.model")(sequelize, Sequelize);
db.employeeRequest = require("../models/employeeRequest.model")(
  sequelize,
  Sequelize
);
db.role = require("../models/role.model")(sequelize, Sequelize);
db.businessUnitManager = require("../models/businessUnitManager.model")(
  sequelize,
  Sequelize
);
db.sectionManager = require("../models/sectionManager.model")(
  sequelize,
  Sequelize
);

db.user.belongsTo(db.employee, { foreignKey: "employeeId" });
db.employee.hasOne(db.user, { foreignKey: "employeeId" });

db.user.belongsTo(db.role, { foreignKey: "roleId" });
db.role.hasMany(db.user, { foreignKey: "roleId" });

db.employee.belongsTo(db.section, { foreignKey: "sectionId" });
db.section.hasMany(db.employee, { foreignKey: "sectionId" });

db.section.belongsTo(db.businessUnit, { foreignKey: "businessId" });
db.businessUnit.hasMany(db.section, { foreignKey: "businessId" });

db.businessUnitManager.belongsTo(db.businessUnit, { foreignKey: "businessId" });
db.businessUnit.hasMany(db.businessUnitManager, { foreignKey: "businessId" });

db.businessUnitManager.belongsTo(db.employee, { foreignKey: "employeeId" });
db.employee.hasOne(db.businessUnitManager, { foreignKey: "employeeId" });

db.sectionManager.belongsTo(db.section, { foreignKey: "sectionId" });
db.section.hasMany(db.sectionManager, { foreignKey: "sectionId" });

db.sectionManager.belongsTo(db.employee, { foreignKey: "employeeId" });
db.employee.hasOne(db.sectionManager, { foreignKey: "employeeId" });

db.employeeRequest.belongsTo(db.employee, { foreignKey: "employeeId" });
db.employee.hasMany(db.employeeRequest, { foreignKey: "employeeId" });

module.exports = db;
