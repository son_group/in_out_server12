module.exports = (sequelize, Sequelize) => {
  const Role = sequelize.define(
    "Role",
    {
      roleId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        field: "role_id",
      },
      roleName: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "role_name",
      },
      description: {
        type: Sequelize.STRING,
      },
    },
    {
      tableName: "roles",
    }
  );

  return Role;
};
